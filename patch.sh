_SDK=$PWD/AmbiqSuiteSDK

# Apply patches.
echo -e "\e[34m --- Applying patches to the boards_sfe...\e[39m"
pushd $_SDK/boards_sfe; git apply ../../patches/boards_sfe_bsp_debug.patch; popd

echo -e "\e[34m --- Applying patches to the SDK...\e[39m"
pushd $_SDK; git apply ../patches/sdk_hal_debug.patch; popd
pushd $_SDK; git apply ../patches/sdk_wsf_trace_complete.patch; popd
