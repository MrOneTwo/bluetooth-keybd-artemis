# source this file to have access to all necessary commands

OLD_PS1="$PS1"
export OLD_PS1
PS1="(apollo) $PS1"
export PS1

#
# Variables below might need to be edited.
#
_TC=~/Downloads/gcc-arm-none-eabi-9-2019-q4-major/bin
_SDK=$PWD/AmbiqSuiteSDK
_DEBUG=~/Downloads/JLink_Linux_V656a_x86_64/

COM_PORT="/dev/ttyUSB0"
export COM_PORT
ASB_UPLOAD_BAUD="115200"
export ASB_UPLOAD_BAUD
SVL_UPLOAD_BAUD="115200"
export SVL_UPLOAD_BAUD
PYTHON3="python3"
export PYTHON3
SDKPATH=$_SDK
export SDKPATH
COMMONPATH="$_SDK/boards_sfe/common/"
export COMMONPATH
BOARD="artemis_thing_plus"
export BOARD
BOARDPATH="$_SDK/boards_sfe/$BOARD/"
export BOARDPATH
PROJECTPATH=$PWD
export PROJECTPATH

PATH="$_TC":$PATH
export PATH

#
# Commands:
#
alias b="make -C $PROJECTPATH/src"
alias bb="make -C $BOARDPATH/bsp/gcc && make -C $SDKPATH/mcu/apollo3/hal/gcc && make -C $PROJECTPATH/src"
alias c="make -C $PROJECTPATH/src clean"
alias cc="make -C $BOARDPATH/bsp/gcc clean && make -C $SDKPATH/mcu/apollo3/hal/gcc clean && make -C $PROJECTPATH/src clean"
alias itm="$_DEBUG/JLinkSWOViewer -device AMA3B1KK-KBR -cpufreq 48000000 -swofreq 2000000"
alias flash="make -C $PROJECTPATH/src bootload"
alias dbc="$_TC/arm-none-eabi-gdb-py -ex \"target remote localhost:2331\" -ex \"monitor SWO EnableTarget 48000000 2000000 1 0\" $PROJECTPATH/src/bin/main_svl.axf"
alias dbs="$_DEBUG/JLinkGDBServer -device AMA3B1KK-KBR -if SWD -speed 4000"

