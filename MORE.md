# Bluetooth keyboard - Artemis

The idea is to make a one handed keyboard, something like Twiddler 3, for CG.

## Materials

- [Sparkfun - developing for ARM](https://learn.sparkfun.com/tutorials/arm-programming)
- [Sparkfun - Apollo3 SDK](https://learn.sparkfun.com/tutorials/using-sparkfun-edge-board-with-ambiq-apollo3-sdk/all)
- [Sparkfun - Artemis BSP](https://github.com/sparkfun/SparkFun_Apollo3_AmbiqSuite_BSPs)

## Toolchain

Grab it from
[here](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).

Don't install it just grab it from that link and use:

```sh
export PATH=~/Downloads/gcc-arm-none-eabi-9-2019-q4-major/bin/:$PATH
```

## Modifying the Makefile

`Makefile` isn't that complicated but few things you should know:

- when adding source files, append them to `SRC`. Be sure the path where that source file is located
  is also appended to the `VPATH`.

## Bootloaders

There are two bootloaders on the *Sparkfun* boards:

- *ASB* - Ambiq Secure Boot, lives in the 0x0 - 0xC000 memory space. This code is physically part of
  the IC.
- *SVL* - Sparkfun Variable Bootloader, starts at 0xC000. After loading the code it will jump to
  0x10000.

## Apollo SDK

#### `makedefs`

The `am_bsp.mk` converts `bsp_pins.src` to `am_bsp_pins.h` and `am_bsp_pins.c` using the
`pinconfig.py` script.

The `bsp_pins.src` file is described in the `Apollo3-GPIO.pdf` and `Apollo3_Porting_Guide.pdf`.

#### `mcu`

Folder with the HAL layer. Here you can look up the API for peripherals like Bluetooth.
The HAL is described in the `Apollo3_Porting_Guide.pdf`.

#### `CMSIS`

The ARM specific layer.

#### `ambiq_ble`

Applications using **Bluetooth**.

#### `utils`

Gives you access to things like `printf`, some debug things, etc.


## Flow graphs

### Advertising and scanning

The slave application first configures the advertising parameters by calling `DmAdvSetInterval()`
to set the advertising interval and then `DmAdvSetData()` twice to set the advertising data and
the scan response data. Then it calls `DmAdvStart()` to start advertising.

The master application configures the scan interval and then calls `DmScanStart()` to begin
scanning. When advertisements are received the stack sends `DM_SCAN_REPORT_IND` events to the
application. The master application stops scanning by calling `DmScanStop()`. The slave application
stops advertising by calling `DmAdvStop()`.

```mermaid
sequenceDiagram
  participant Master_Application
  participant Master_Stack
  participant Slave_Stack
  participant Slave_Application
  Master_Application->>Master_Stack: DmScanSetInterval()
  Master_Application->>Master_Stack: DmScanStart()
  Master_Stack->>Master_Application: DM_SCAN_START_IND
  Note right of Master_Stack: Scanning started
  Note left of Master_Stack: Scanning started
  Slave_Application->>Slave_Stack: DmAdvSetInterval()
  Slave_Application->>Slave_Stack: DmAdvSetData() (advertising data)
  Slave_Application->>Slave_Stack: DmAdvSetData() (scan response data)
  Slave_Application->>Slave_Stack: DmAdvStart()
  Slave_Stack->>Slave_Application: DM_ADV_START_IND
  Note left of Slave_Stack: Advertising started
  Note right of Slave_Stack: Advertising started
  Master_Stack->>Master_Application: DM_SCAN_REPORT_IND
  Master_Stack->>Master_Application: DM_SCAN_REPORT_IND
  Master_Application->>Master_Stack: DmScanStop()
  Master_Stack->>Master_Application: DM_SCAN_SCAN_STOP_IND
  Note right of Master_Stack: Scanning stopped
  Slave_Application->>Slave_Stack: DmAdvStop()
  Slave_Stack->>Slave_Application: DM_ADV_STOP_IND
  Note left of Slave_Stack: Advertising stopped
  Note left of Master_Stack: Scanning stopped
  Slave_Application->>Slave_Stack: DmAdvStop()
  Slave_Stack->>Slave_Application: DM_ADV_STOP_IND
  Note right of Slave_Stack: Advertising stopped
```

### Connection open and close

The scenario starts with the slave device advertising and the master device already having the
address of the slave. The master application calls `DmConnOpen()` to initiate a connection. A
connection is established and a `DM_CONN_OPEN_IND` is sent to the application from the stack on
each device.

Next, the master performs a connection update by calling `DmConnUpdate()`. When the connection
update is complete `DM_CONN_UPDATE_IND` is sent to the application from the stack on each device.

The slave can choose to close the connection by calling `DmConnClose()`. A `DM_CONN_CLOSE_IND`
event is sent from the stack on each device when the connection is closed.

```mermaid
sequenceDiagram
  participant Master_Application
  participant Master_Stack
  participant Slave_Stack
  participant Slave_Application
  Note right of Slave_Stack: Advertising started
  Master_Application->>Master_Stack: DmConnOpen()
  Note right of Master_Stack: Connection established
  Slave_Stack->>Slave_Application: DM_CONN_OPEN_IND
  Master_Stack->>Master_Application: DM_CONN_OPEN_IND
  Master_Application->>Master_Stack: DmConnUpdate()
  Note right of Master_Stack: Connection update
  Slave_Stack->>Slave_Application: DM_CONN_UPDATE_IND
  Master_Stack->>Master_Application: DM_CONN_UPDATE_IND
  Slave_Application->>Slave_Stack: DmConnClose()
  Note right of Master_Stack: Connection closed
  Slave_Stack->>Slave_Application: DM_CONN_CLOSE_IND
  Master_Stack->>Master_Application: DM_CONN_CLOSE_IND
```

### Pairing

A connection is established between the two devices and the master application initiates pairing by
calling `DmSecPairReq()`. The slave application receives a `DM_SEC_PAIR_IND` and calls
`DmSecPairRsp()` to proceed with pairing. In this example a PIN is used and a `DM_SEC_AUTH_REQ_IND`
is sent to the application on each device to request a PIN. Each application responds with the PIN
by calling `DmSecAuthRsp()`.

In the next phase of pairing the connection is encrypted and a `DM_SEC_ENCRYPT_IND` event is sent
to the application on each device. Then key exchange begins. According to the Bluetooth
specification, the slave device always distributes keys first. In this example, the slave
distributes two keys and the master device distributes one. The slave sends its key data to the
master. Note that when the slave sends its LTK (Long Term Key), the slave application receives a
`DM_SEC_KEY_IND` containing its own LTK. Then the master sends its key data to the slave. When the
key exchange is completed successfully, a `DM_SEC_PAIR_CMPL_IND` event is sent to the application
on each device.

Additionally the IRK (Identity Resolving Key) needs to be shared for future RPA (Resolvable Private
Address - randomized address) resolving.

```mermaid
sequenceDiagram
  participant Master_Application
  participant Master_Stack
  participant Slave_Stack
  participant Slave_Application
  Note right of Master_Stack: Connection established
  Master_Application->>Master_Stack: DmSecPairReq()
  Note right of Master_Stack: Pairing request
  Slave_Stack->>Slave_Application: DM_SEC_PAIR_IND
  Slave_Application->>Slave_Stack: DmSecPairRsp()
  Note right of Master_Stack: Pairing response
  Master_Stack->>Master_Application: DM_SEC_AUTH_REQ_IND
  Slave_Stack->>Slave_Application: DM_SEC_AUTH_REQ_IND
  Master_Application->>Master_Stack: DmSecAuthRsp()
  Slave_Application->>Slave_Stack: DmSecAuthRsp()
  Note right of Master_Stack: Connection encrypted
  Master_Stack->>Master_Application: DM_SEC_ENCRYPT_IND
  Slave_Stack->>Slave_Application: DM_SEC_ENCRYPT_IND
  Note right of Master_Stack: Begin key exchange
  Slave_Stack->>Master_Stack: key data (LTK)
  Master_Stack->>Master_Application: DM_SEC_KEY_IND
  Slave_Stack->>Slave_Application: DM_SEC_KEY_IND
  Slave_Stack->>Master_Stack: key data (IRK)
  Master_Stack->>Master_Application: DM_SEC_KEY_IND
  Master_Stack->>Slave_Stack: key data (IRK)
  Slave_Stack->>Slave_Application: DM_SEC_KEY_IND
  Note right of Master_Stack: Key exchange complete
  Master_Stack->>Master_Application: DM_SEC_PAIR_CMPL_IND
  Slave_Stack->>Slave_Application: DM_SEC_PAIR_CMPL_IND
```
