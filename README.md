# Implementing BLE HID on Apollo3

**This is very much work in progress!**

This is my attempt at implementing a wireless BLE HID device on a Apollo3 chip. The *SDK* doesn't
make it that easy to implement. My goal is to document the process and make this process a breeze
for anyone else.

If you want to understand more about this project, I recommend reading through *MORE.md*.
