#ifndef HIDAPP_API_H
#define HIDAPP_API_H

#ifdef __cplusplus
extern "C" {
#endif

// HID Usages for a mouse.

// HID Usages for a keyboard.
enum
{
  KEYBD_NONE = 0x00,
  KEYBD_ERR_ROLL_OVER = 0x01,
  KEYBD_POST_FAIL = 0x02,
  KEYBD_ERR_UNDEFINED = 0x03,
  KEYBD_A = 0x04,
  KEYBD_B = 0x05,
  KEYBD_C = 0x06,
  KEYBD_D = 0x07,
  KEYBD_E = 0x08,
  KEYBD_F = 0x09,
  KEYBD_G = 0x0A,
  KEYBD_H = 0x0B,
  KEYBD_I = 0x0C,
  KEYBD_J = 0x0D,
  KEYBD_K = 0x0E,
  KEYBD_L = 0x0F,
  KEYBD_M = 0x10,
  KEYBD_N = 0x11,
  KEYBD_O = 0x12,
  KEYBD_P = 0x13,
  KEYBD_Q = 0x14,
  KEYBD_R = 0x15,
  KEYBD_S = 0x16,
  KEYBD_T = 0x17,
  KEYBD_U = 0x18,
  KEYBD_V = 0x19,
  KEYBD_W = 0x1A,
  KEYBD_X = 0x1B,
  KEYBD_Y = 0x1C,
  KEYBD_Z = 0x1D,
  KEYBD_1 = 0x1E,
  KEYBD_2 = 0x1F,
  KEYBD_3 = 0x20,
  KEYBD_4 = 0x21,
  KEYBD_5 = 0x22,
  KEYBD_6 = 0x23,
  KEYBD_7 = 0x24,
  KEYBD_8 = 0x25,
  KEYBD_9 = 0x26,
  KEYBD_0 = 0x27,
  KEYBD_RETURN = 0x28,
  KEYBD_ENTER = 0x29,
  KEYBD_DELETE = 0x2A,
  KEYBD_TAB = 0x2B,
  KEYBD_SPACE = 0x2C,
  KEYBD_MINUS = 0x2D,
  KEYBD_EQUALS = 0x2E,
  KEYBD_SQUARE_L = 0x2F,
  KEYBD_SQUARE_R = 0x30,
  KEYBD_BACK_SLASH = 0x31,
  KEYBD_HASH = 0x32,
  KEYBD_SEMICOLON = 0x33,
  KEYBD_QUOTE = 0x34,
  KEYBD_GRAVE = 0x35,
  KEYBD_COMMA = 0x36,
  KEYBD_DOT = 0x37,
  KEYBD_FORWARD_SLASH = 0x38,
  KEYBD_CAPS_LOCK = 0x39,
  KEYBD_F1 = 0x3A,
  KEYBD_F2 = 0x3B,
  KEYBD_F3 = 0x3C,
  KEYBD_F4 = 0x3D,
  KEYBD_F5 = 0x3E,
  KEYBD_F6 = 0x3F,
  KEYBD_F7 = 0x40,
  KEYBD_F8 = 0x41,
  KEYBD_F9 = 0x42,
  KEYBD_F10 = 0x43,
  KEYBD_F11 = 0x44,
  KEYBD_F12 = 0x45,
  KEYBD_PRINT_SCREEN = 0x46,
  KEYBD_SCROLL_LOCK = 0x47,
  KEYBD_PAUSE = 0x48,
  KEYBD_INSERT = 0x49,
  KEYBD_HOME = 0x4A,
  KEYBD_PAGE_UP = 0x4B,
  KEYBD_DELETE_FORWARD = 0x4C,
  KEYBD_END = 0x4D,
  KEYBD_PAGE_DOWN = 0x4E,
  KEYBD_RIGHT_ARROW = 0x4F,
  KEYBD_LEFT_ARROW = 0x50,
  KEYBD_DOWN_ARROW = 0x51,
  KEYBD_UP_ARROW = 0x52,
  KEYBD_PAD_NUM_LOCK = 0x53,
  KEYBD_PAD_DIVIDE = 0x54,
  KEYBD_PAD_MULTIPLY = 0x55,
  KEYBD_PAD_MINUS = 0x56,
  KEYBD_PAD_PLUS = 0x57,
  KEYBD_PAD_ENTER = 0x58,
  KEYBD_PAD_1 = 0x59,
  KEYBD_PAD_2 = 0x5A,
  KEYBD_PAD_3 = 0x5B,
  KEYBD_PAD_4 = 0x5C,
  KEYBD_PAD_5 = 0x5D,
  KEYBD_PAD_6 = 0x5E,
  KEYBD_PAD_7 = 0x5F,
  KEYBD_PAD_8 = 0x60,
  KEYBD_PAD_9 = 0x61,
  KEYBD_PAD_0 = 0x62,
  KEYBD_PAD_DOT = 0x63,

  KEYBD_APPLICATION = 0x65,
  KEYBD_POWER = 0x66,
  KEYBD_PAD_EQUALS = 0x67,
  KEYBD_F13 = 0x68,
  KEYBD_F14 = 0x69,
  KEYBD_F15 = 0x6A,
  KEYBD_F16 = 0x6B,
  KEYBD_F17 = 0x6C,
  KEYBD_F18 = 0x6D,
  KEYBD_F19 = 0x6E,
  KEYBD_F20 = 0x6F,
  KEYBD_F21 = 0x70,
  KEYBD_F22 = 0x71,
  KEYBD_F23 = 0x72,
  KEYBD_F24 = 0x73,
  /*
  0x64 Keyboard Non-US \ and |
  0x74 Keyboard Execute
  0x75 Keyboard Help
  0x76 Keyboard Menu
  0x77 Keyboard Select
  0x78 Keyboard Stop
  0x79 Keyboard Again
  0x7A Keyboard Undo
  0x7B Keyboard Cut
  0x7C Keyboard Copy
  0x7D Keyboard Paste
  0x7E Keyboard Find
  0x7F Keyboard Mute
  0x80 Keyboard Volume Up
  0x81 Keyboard Volume Down
  0x82 Keyboard Locking Caps Lock
  0x83 Keyboard Locking Num Lock
  0x84 Keyboard Locking Scroll Lock
  0x85 Keypad Comma
  0x86 Keypad Equal Sign
  0x87 Keyboard International1
  0x88 Keyboard International2
  0x89 Keyboard International3
  0x8A Keyboard International4
  0x8B Keyboard International5
  0x8C Keyboard International6
  0x8D Keyboard International7
  0x8E Keyboard International8
  0x8F Keyboard International9
  0x90 Keyboard LANG1
  0x91 Keyboard LANG2
  0x92 Keyboard LANG3
  0x93 Keyboard LANG4
  0x94 Keyboard LANG5
  0x95 Keyboard LANG6
  0x96 Keyboard LANG7
  0x97 Keyboard LANG8
  0x98 Keyboard LANG9
  0x99 Keyboard Alternate Erase
  0x9A Keyboard SysReq/Attention
  0x9B Keyboard Cancel
  0x9C Keyboard Clear
  0x9D Keyboard Prior
  0x9E Keyboard Return
  0x9F Keyboard Separator
  0xA0 Keyboard Out
  0xA1 Keyboard Oper
  0xA2 Keyboard Clear/Again
  0xA3 Keyboard CrSel/Props
  0xA4 Keyboard ExSel
  */
  KEYBD_MOD_CTRL_LEFT_MASK   = 1 << 0,
  KEYBD_MOD_SHIFT_LEFT_MASK  = 1 << 1,
  KEYBD_MOD_ALT_LEFT_MASK    = 1 << 2,
  KEYBD_MOD_META_LEFT_MASK   = 1 << 3,
  KEYBD_MOD_CTRL_RIGHT_MASK  = 1 << 4,
  KEYBD_MOD_SHIFT_RIGHT_MASK = 1 << 5,
  KEYBD_MOD_ALT_RIGHT_MASK   = 1 << 6,
  KEYBD_MOD_META_RIGHT_MASK  = 1 << 7,
};

void HidAppStart(void);
void HidAppHandlerInit(wsfHandlerId_t handlerId);
void HidAppHandler(wsfEventMask_t event, wsfMsgHdr_t *pMsg);

void hidAppKeyboardReportEvent(uint8_t modifiers, uint8_t keys[], uint8_t numKeys);

#ifdef __cplusplus
};
#endif

#endif /* HIDAPP_API_H */


