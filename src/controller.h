//
// The idea here is to represent a controller state.
// 

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <stdint.h>
#include <stdbool.h>

#define CTRLR_BUTTONS_COUNT (9U)

// TODO(michalc): this describes a keyboard and not the final controller.
typedef struct Controller
{
  uint8_t buttons[CTRLR_BUTTONS_COUNT];
  uint8_t modifiers;
} Controller;

void UpdateController(Controller* const ctrlr);
bool CompareControllerStates(const Controller c1, const Controller c2);

#endif  // CONTROLLER_H
