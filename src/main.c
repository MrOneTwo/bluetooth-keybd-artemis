#include "controller.h"

#include <stdint.h>
#include <stdbool.h>

#include "wsf_types.h"
#include "wsf_trace.h"
#include "wsf_buf.h"
#include "wsf_msg.h"

#include "hci_handler.h"
#include "dm_handler.h"
#include "l2c_handler.h"
#include "att_handler.h"
#include "smp_handler.h"
#include "l2c_api.h"
#include "att_api.h"
#include "smp_api.h"
#include "app_api.h"
#include "hci_core.h"
#include "hci_drv.h"
#include "hci_drv_apollo.h"
#include "hci_drv_apollo3.h"

#include "am_util.h"
#include "am_bsp.h"

#include "hidapp_api.h"
#include "app_ui.h"

/*
 * 1/512Hz = 0.002s - that means that 1ms (T = 0.001s) requies 0.5 ticks of a 512Hz clock.
 * If CLK_TICKS_PER_WSF_TICKS == 5 then WSF runs on a 102Hz clock.
 * 1/100Hz = 0.01s - that means that 1ms (T = 0.001s) requires 0.1 ticks of a 100Hz clock.
 * Default WSF_MS_PER_TICK is 10. That would mean that it assumes a 100Hz clock.
 */

// I think this is the WSF_MS_PER_TICK that's in the wsf_timer.h.
#define MS_PER_TIMER_TICK        10  // Miliseconds per WSF tick.
#define CLK_TICKS_PER_WSF_TICKS  5   // Number of CTIMER counts per WSF tick.

#define CONT_TIMER_PERIOD (0x10000) // 65536 - 16 bit timer

void scheduler_timer_init(void);
void set_next_wakeup(void);
void radio_timer_handler(void);
void exactle_stack_init(void);
void update_scheduler_timers(void);

#define MEMORY_BLOB_SIZE (8192 + 2048)
#define MEMORY_POOLS_COUNT 8
uint8_t memoryPool[MEMORY_BLOB_SIZE];
uint32_t lastTime;
bool bleON = false;
wsfHandlerId_t hidhandlerId;

// Buttons timer and handler.
wsfHandlerId_t ButtonHandlerId;
wsfTimer_t ButtonTimer;
#define BUTTON_TIMER_EVENT 0x80

wsfTimer_t PowerCycleTimer;
#define POWERCYCLE_TIMER_EVENT 0x40

am_hal_wdt_config_t wd = {};

static Controller controller;
static Controller controllerPrev;

bool ledState = true;

void
scheduler_timer_init(void)
{
  // One-shot timer that provides interrupts for scheduled events.
  // Using LFRC (1kHz; always on clock) with an output of 512Hz.
  am_hal_ctimer_clear(0, AM_HAL_CTIMER_TIMERA);
  am_hal_ctimer_config_single(0, AM_HAL_CTIMER_TIMERA,
                              (AM_HAL_CTIMER_INT_ENABLE |
                               AM_HAL_CTIMER_LFRC_512HZ |
                               AM_HAL_CTIMER_FN_ONCE));
  // Continous timer which provides a constant time-base.
  am_hal_ctimer_clear(0, AM_HAL_CTIMER_TIMERB);
  am_hal_ctimer_config_single(0, AM_HAL_CTIMER_TIMERB,
                              (AM_HAL_CTIMER_LFRC_512HZ |
                              AM_HAL_CTIMER_FN_CONTINUOUS));
  // Enable the timer interrupt.
  am_hal_ctimer_int_register(AM_HAL_CTIMER_INT_TIMERA0, radio_timer_handler);
  am_hal_ctimer_int_enable(AM_HAL_CTIMER_INT_TIMERA0);
  // Enable the core interrupt.
  NVIC_EnableIRQ(CTIMER_IRQn);
  // Start the continous timer.
  am_hal_ctimer_start(0, AM_HAL_CTIMER_TIMERB);
}

void
set_next_wakeup(void)
{
  bool_t timerRunning;
  wsfTimerTicks_t nextExpiration;

  am_hal_ctimer_stop(0, AM_HAL_CTIMER_TIMERA);
  am_hal_ctimer_clear(0, AM_HAL_CTIMER_TIMERA);

  // Check if there are any timers in the timers queue and fetch the first timer's
  // expiration. The timers are sorted so fetching the first one is legit.
  nextExpiration = WsfTimerNextExpiration(&timerRunning);

  // If there is a pending WSF timer event, set an interrupt to wake up in time
  // to service it. Otherwise, set an interrupt to wake us up in time to prevent
  // a double-overflow of the continous timer.
  if (nextExpiration)
  {
    am_hal_ctimer_period_set(0, AM_HAL_CTIMER_TIMERA,
                             nextExpiration * CLK_TICKS_PER_WSF_TICKS, 0);
  }
  else
  {
    am_hal_ctimer_period_set(0, AM_HAL_CTIMER_TIMERA, CONT_TIMER_PERIOD / 2, 0);
  }

  am_hal_ctimer_start(0, AM_HAL_CTIMER_TIMERA);
}

void
radio_timer_handler(void)
{
  // Set the task used by the given handler as ready to run.
  ledState = !ledState;
  ledState ?
    am_devices_led_on(am_bsp_psLEDs, 0) :
    am_devices_led_off(am_bsp_psLEDs, 0);
}

void
update_scheduler_timers(void)
{
  uint32_t currentTime, elapsedTime;

  currentTime = am_hal_ctimer_read(0, AM_HAL_CTIMER_TIMERB);

  // Compute how much time has passed since the last continous timer read.
  // Read should be done often enough that there shouldn't be more than one
  // overflow.
  elapsedTime = currentTime >= lastTime ?
    (currentTime - lastTime) : (CONT_TIMER_PERIOD - lastTime + currentTime);

  // Check to see if any WSF ticks need to happen.
  if ((elapsedTime / CLK_TICKS_PER_WSF_TICKS) > 0)
  {
    // Iterate over the WSF timers and save the current time as the last update.
    WsfTimerUpdate(elapsedTime / CLK_TICKS_PER_WSF_TICKS);
    lastTime = currentTime;
  }
}

void
button_handler(wsfEventMask_t event, wsfMsgHdr_t *pMsg)
{
  // Update buttons state. There is some debouncing logic in there.
  am_devices_button_array_tick(am_bsp_psButtons, AM_BSP_NUM_BUTTONS);

  // Handle direct events.
  if ((event & BUTTON_TIMER_EVENT) || (pMsg->event & BUTTON_TIMER_EVENT))
  {
    // Restart the button timer.
    WsfTimerStartMs(&ButtonTimer, 10);

    UpdateController(&controller);
    bool controllerStateChange = CompareControllerStates(controller, controllerPrev);

    if (controllerStateChange)
    {
      hidAppKeyboardReportEvent(controller.modifiers, controller.buttons, AM_BSP_NUM_BUTTONS);
    }

    controllerPrev = controller;
  }

  if ((event & POWERCYCLE_TIMER_EVENT) || (pMsg->event & POWERCYCLE_TIMER_EVENT))
  {
    if (bleON == true)
    {
      dmConnId_t connId = AppConnIsOpen();

      if (connId != DM_CONN_ID_NONE)
      {
        AppConnClose(connId);
        return;
      }

      if (AppSlaveIsAdvertising() == true)
      {
        AppAdvStop();
        return;
      }

      am_util_debug_printf(" >> Power off Apollo3 BLE controller\n");
      HciDrvRadioShutdown();
      bleON = false;
    }
    else
    {
      am_util_debug_printf(" >> Power on Apollo3 BLE controller\n");
      HciDrvRadioBoot(1);
      DmDevReset();
      bleON = true;
    }
  }
}

void
exactle_stack_init(void)
{
  wsfHandlerId_t handlerId;

  // Each "module" stores handlerId in it's internal global variable. It gets
  // copied when callin `...HandlerInit()`.
  // Handler function in each module analyzes `event` field of the
  // `wsfMsgHdr_t *pMsg` argument. It also analyzes the first argument which is
  // `wsfEventMask_t event`.

  // Initialize a buffer pool for WSF dynamic memory needs.
  wsfBufPoolDesc_t memoryDescription[MEMORY_POOLS_COUNT] = {
    { 16, 128},  // 2k
    { 32,  64},  // 2k
    { 16,  64},  // 1k
    { 32,  32},  // 1k
    { 32,  32},  // 1k
    { 64,  16},  // 1k
    {128,   8},  // 1k
    {258,   4},  // 1k
  };
  WsfBufInit(MEMORY_BLOB_SIZE, memoryPool, MEMORY_POOLS_COUNT, memoryDescription);

  // Initialize security.
  SecInit();
  SecAesInit();
  SecCmacInit();
  SecEccInit();

  /*
   * CORDIO STACK
   * - sw/stack
   * - sw/hci - binds HCI to the Apollo3 specific HCI via HAL.
   */

  // Set up callback functions for the various layers of the ExactLE stack.
  handlerId = WsfOsSetNextHandler(HciHandler);
  HciHandlerInit(handlerId);

  handlerId = WsfOsSetNextHandler(HciDrvHandler);
  HciDrvHandlerInit(handlerId);

  handlerId = WsfOsSetNextHandler(L2cSlaveHandler);
  L2cSlaveHandlerInit(handlerId);
  L2cInit();
  L2cSlaveInit();

  // All the functions below are defined in the libstacklib.a
  // Set up device manager.
  handlerId = WsfOsSetNextHandler(DmHandler);
  DmDevVsInit(0);
  DmAdvInit();
  DmConnInit();
  DmConnSlaveInit();
  DmSecInit();
  DmSecLescInit();
  DmPrivInit();
  DmHandlerInit(handlerId);

  handlerId = WsfOsSetNextHandler(AttHandler);
  AttHandlerInit(handlerId);
  AttsInit();
  AttsIndInit();
  AttcInit();

  handlerId = WsfOsSetNextHandler(SmpHandler);
  SmpHandlerInit(handlerId);
  SmprInit();
  SmprScInit();
  HciSetMaxRxAclLen(251);

  /*
   * APP FRAMEWORK
   */

  handlerId = WsfOsSetNextHandler(AppHandler);
  AppHandlerInit(handlerId);

  handlerId = WsfOsSetNextHandler(HidAppHandler);
  hidhandlerId = handlerId;
  HidAppHandlerInit(handlerId);
}


int
main(void)
{
  am_util_id_t sIdDevice;
  uint32_t ui32StrBuf;

  wd.ui32Config = AM_HAL_WDT_LFRC_CLK_16HZ | AM_HAL_WDT_ENABLE_RESET | AM_HAL_WDT_ENABLE_INTERRUPT;
  wd.ui16InterruptCount = 16U;
  wd.ui16ResetCount = 16U;

  // Set the clock frequency.
  am_hal_clkgen_control(AM_HAL_CLKGEN_CONTROL_SYSCLK_MAX, 0);

  // Set the default cache configuration
  am_hal_cachectrl_config(&am_hal_cachectrl_defaults);
  am_hal_cachectrl_enable();

  am_hal_wdt_init(&wd);

#ifndef NOFPU
  am_hal_sysctrl_fpu_enable();
  am_hal_sysctrl_fpu_stacking_enable(true);
#else
  am_hal_sysctrl_fpu_disable();
#endif

  // Disable XTAL (32kHz), use LFRC for the RTC, turn RTC off.
  am_bsp_low_power_init();

  // Enables ITM and TPIU via HAL layer.
  am_bsp_itm_printf_enable();

  // Disable unnecessary peripherals.
  //am_hal_rtc_osc_disable();
  if (am_hal_pwrctrl_periph_disable(AM_HAL_PWRCTRL_PERIPH_UART0) == AM_HAL_STATUS_SUCCESS)
  {
    am_util_debug_printf("[INFO] Turned off the UART0\n");
  }
  else
  {
    am_util_debug_printf("[WARN] Failed to turn off the UART0\n");
  }
  if (am_hal_pwrctrl_periph_disable(AM_HAL_PWRCTRL_PERIPH_UART1) == AM_HAL_STATUS_SUCCESS)
  {
    am_util_debug_printf("[INFO] Turned off the UART1\n");
  }
  else
  {
    am_util_debug_printf("[WARN] Failed to turn off the UART1\n");
  }
  if (am_hal_pwrctrl_periph_disable(AM_HAL_PWRCTRL_PERIPH_SCARD) == AM_HAL_STATUS_SUCCESS)
  {
    am_util_debug_printf("[INFO] Turned off the SCARD\n");
  }
  else
  {
    am_util_debug_printf("[WARN] Failed to turn off the SCARD\n");
  }

  // Clear the Timers queue.
  WsfTimerInit();
  // Starts the tasks and their timers.
  exactle_stack_init();
  // Start the scheduler timers.
  scheduler_timer_init();

  // Setup the buttons pins and turn the pins off. They are turned on when reading their state.
  am_devices_button_array_init(am_bsp_psButtons, AM_BSP_NUM_BUTTONS);

  ButtonHandlerId = WsfOsSetNextHandler(button_handler);
  ButtonTimer.handlerId = ButtonHandlerId;
  PowerCycleTimer.handlerId = ButtonHandlerId;

  WsfSetEvent(ButtonHandlerId, POWERCYCLE_TIMER_EVENT);
  ButtonTimer.msg.event = BUTTON_TIMER_EVENT;
  WsfTimerStartMs(&ButtonTimer, 1);
  wsfOsDispatcher();

  am_hal_gpio_pinconfig(am_bsp_psLEDs[0].ui32GPIONumber, g_AM_HAL_GPIO_OUTPUT);
  am_devices_led_on(am_bsp_psLEDs, 0);

  // Let's get this PARTY STARTED!
  HidAppStart();

  while (1)
  {
    // Compute elapsed time in the continous timer. This also calls
    // WsfTimerUpdate(wsfTimerTicks_t ticks) which goes through the timers queue
    // and decrements ticks for each timer with the elapsed time. If the ticks
    // for a timer in the queue is 0 then the wsfOs.task gets its
    // WSF_TIMER_EVENT flag set.
    update_scheduler_timers();
    // Dispatcher handles:
    // - WSF_MSG_QUEUE_EVENT - by calling the handler with args event = 0, and
    // message from the `task` message queue.
    // - WSF_TIMER_EVENT - by calling the handler with args event = 0, and
    // message from the `task` message queue. The message doesn't get freed.
    // - WSF_HANDLER_EVENT - by calling the handler with the event mask that
    // corresponds to this handler and message = NULL.
    wsfOsDispatcher();

    // Set wakeup when the timer of a task reaches 0, or so a double overflow of
    // the continous timer doesn't happen.
    set_next_wakeup();
    am_hal_interrupt_master_disable();
    
    // Go to sleep.
    am_bsp_itm_printf_disable();
    if(wsfOsReadyToSleep())
    {
      // Put the CORE to sleep.
      am_hal_sysctrl_sleep(AM_HAL_SYSCTRL_SLEEP_DEEP);
    }
    am_bsp_itm_printf_enable();

    // - copy PRIMASK register into r0
    // - clear PRIMASK with `cpsie i`
    // - branch into the return address
    am_hal_interrupt_master_enable();
  }
}

/*
 * Override the symbols from the startup_gcc.
 */
void
am_ctimer_isr(void)
{
  // Get the status of the timer interrupt.
  // `true` means only enabled interrupts.
  const uint32_t status = am_hal_ctimer_int_status_get(true);
  am_hal_ctimer_int_clear(status);
  am_hal_ctimer_int_service(status);
}

void
am_ble_isr(void)
{
  HciDrvIntService();
}
