//
// The idea here is to represent a controller state.
// 

#ifndef DM_API_STRING_H
#define DM_API_STRING_H

#include <stdint.h>

const char* const dmEventToString(const uint32_t i);

#endif  // DM_API_STRING_H
