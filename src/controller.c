#include "controller.h"

#include "wsf_types.h"
#include "wsf_trace.h"
#include "wsf_buf.h"
#include "wsf_msg.h"

#include "hidapp_api.h"
#include "am_util.h"
#include "am_bsp.h"

typedef enum ButtonRole
{
  KEY,
  MODIFIER,
  MACRO
} ButtonRole;

typedef struct ButtonFunction
{
  ButtonRole role;
  union {
    uint8_t key;
    uint8_t modifiersMask;
  };
} ButtonFunction;

static ButtonFunction buttonsFunction[CTRLR_BUTTONS_COUNT] = {
  {.role = KEY, .key = KEYBD_NONE},
  {.role = MODIFIER, .modifiersMask = KEYBD_MOD_CTRL_LEFT_MASK},
  {.role = KEY, .key = KEYBD_B},
  {.role = MODIFIER, .modifiersMask = KEYBD_MOD_SHIFT_LEFT_MASK},
  {.role = MODIFIER, .modifiersMask = KEYBD_MOD_ALT_LEFT_MASK},
  {.role = KEY, .key = KEYBD_Q},
  {.role = KEY, .key = KEYBD_A},
  {.role = KEY, .key = KEYBD_A},
  {.role = KEY, .key = KEYBD_A},
};


// Poll the hardware state via HAL and store the state in a structure.
void UpdateController(Controller* const ctrlr)
{
  for (uint8_t bi = 0; bi < AM_BSP_NUM_BUTTONS; bi++)
  {
    if (am_devices_button_pressed(am_bsp_psButtons[bi]))
    {
      am_util_debug_printf(" >> Got Button %d Press\n", bi);
      switch (buttonsFunction[bi].role)
      {
        case KEY: ctrlr->buttons[bi] = buttonsFunction[bi].key; break;
        case MODIFIER: ctrlr->modifiers |= buttonsFunction[bi].modifiersMask; break;
        case MACRO: break;
      }
    }
    else if (am_devices_button_released(am_bsp_psButtons[bi]))
    {
      am_util_debug_printf(" >> Got Button %d Released\n", bi);
      switch (buttonsFunction[bi].role)
      {
        case KEY: ctrlr->buttons[bi] = KEYBD_NONE; break;
        case MODIFIER: ctrlr->modifiers &= ~(buttonsFunction[bi].modifiersMask); break;
        case MACRO: break;
      }
    }
  }
}

bool CompareControllerStates(const Controller c1, const Controller c2)
{
  bool differ = false;

  uint8_t i = 0;
  for (; i < CTRLR_BUTTONS_COUNT; i++)
  {
    if (c1.buttons[i] != c2.buttons[i])
    {
      differ = true;
      break;
    }
  }

  if (c1.modifiers != c2.modifiers)
  {
    differ = true;
  }

  return differ;
}
